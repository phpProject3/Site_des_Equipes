<?php
/*
 * On indique que les chemins des fichiers qu'on inclut
 * seront relatifs au répertoire src.
 */
set_include_path("./src");

/* Inclusion des classes utilisées dans ce fichier */
require_once('/users/benfara211/private/mysql_config.php');
require_once("lib/ObjectFileDB.php");
require_once("model/Equipe.php");
require_once("model/EquipeStorage.php");
require_once("model/EquipeStorageFile.php");
require_once("model/EquipeStorageMySQL.php");
require_once("Router.php");

$dsn = "mysql:host=".MYSQL_HOST.";dbname=".MYSQL_DB.";charset=utf8";
$user = MYSQL_USER;
$pass = MYSQL_PASSWORD;
$pdo = new PDO($dsn,$user,$pass);
/*
 * Cette page est simplement le point d'arrivée de l'internaute
 * sur notre site. On se contente de créer un routeur
 * et de lancer son main.
 */
//$e = new EquipeStorageFile();
$e = new EquipeStorageMySQL($pdo);
$router = new Router();
$router->main($e);
?>