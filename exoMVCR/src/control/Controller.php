<?php

/* Inclusion des classes nécessaires */
require_once("view/View.php");
require_once("model/Equipe.php");
require_once("model/EquipeStorage.php");
require_once("model/EquipeBuilder.php");


class Controller {

	protected $view;
	protected $equipeTab;


	public function __construct(View $view,EquipeStorage $equipeTab ) {
		$this->view = $view;
        $this->equipeTab =$equipeTab;

	}

    public function showList($bd,$tri)
    {   
		if($tri==-1)
		  arsort($bd);
		  if($tri==1)
		   sort($bd);
		//var_dump($bd);
        $this->view->makeListPage($bd);
    }

	public function showInformation($id) {
		$equipe=$this->equipeTab->read($id);
        
		if ($equipe !== null){ 
			/* L'equipe existe, on prépare la page */
            $this->view->makeEquipePage($id,$equipe);
	    }
        else {
            $this->view->makeUnknownEquipePage();
        }
    }
    
    public function newEquipe() {
		/* Affichage du formulaire de création
		* avec les données par défaut. */
		$equipeb = key_exists('currentNewEquipe', $_SESSION) ? ( $_SESSION['currentNewEquipe']==='' ? new EquipeBuilder() : $_SESSION['currentNewEquipe'] ) : new EquipeBuilder();
		$this->view->makeEquipeCreationPage($equipeb);
	}

    public function saveNewEquipe(array $data) {
       
        $equipeb= new EquipeBuilder($data);
        if(!$equipeb->isValid())
        //$this->view->makeUnexpectedErrorPage("entree non valide");
        {   //$data['errors']=$errors;
			$_SESSION['currentNewEquipe']=$equipeb;
            //$this->view->makeEquipeCreationPage($equipeb);
			$this->view->displayEquipeCreationFailure();
		 }  
        else
        { 
        $equipe= $equipeb->createEquipe();
        $id=$this->equipeTab->create($equipe);
		unset($_SESSION['currentNewEquipe']);
        //echo $id . " => " .$equipe->getNom();
		$this->view->displayEquipeCreationSuccess($id);
		
       // $this->view->makeEquipePage($id,$equipe);
	    }

    }
   
    public function askEquipeDeletion($id) {
		/* On récupère l equipe en BD */
		$equipe = $this->equipeTab->read($id);
		if ($equipe === null) {
			/* L equipe n'existe pas en BD */
			$this->view->makeUnknownEquipePage();
		} else {
			/* L equipe existe, on prépare la page */
			$this->view->makeEquipeDeletionPage($id,$equipe);
		}
	}

    public function deleteEquipe($id){
        $equipe = $this->equipeTab->read($id);
		if ($equipe === null) {
            /* L'equipe n'existe pas en BD */
			$this->view->makeUnknownEquipePage();
        }
        else
        {
            $this->equipeTab->delete($id);
			$this->view->displayEquipeDeletedPage($id);
           // $this->view->makeEquipeDeletedPage();

        }


    }
    

    public function modifyEquipe($id) {
		/* On récupère en BD l equipe à modifier */
		$e = $this->equipeTab->read($id);
		if ($e === null) {
			$this->view->makeUnknownEquipePage();
		} else {
			/* Extraction des données modifiables */
			//$cf = EquipeBuilder::buildFromEquipe($e);
			$cf= key_exists('currentNewEquipe', $_SESSION) ? ( $_SESSION['currentNewEquipe']==='' ? EquipeBuilder::buildFromEquipe($e) : $_SESSION['currentNewEquipe'] ) : EquipeBuilder::buildFromEquipe($e);

			/* Préparation de la page de formulaire */
			$this->view->makeEquipeModifPage($id, $cf);
		}
	}


    public function saveEquipeModifications($id, array $data) {
		/* On récupère en BD la equipe à modifier */
		$e = $this->equipeTab->read($id);        
		if ($e === null) {
			$this->view->makeUnknownEquipePage();
		} else {
			$cf = new EquipeBuilder($data);
			/* Validation des données */
			if ($cf->isValid()) {
				/* Modification de l equipe */
				$cf->updateEquipe($e);
				/* On essaie de mettre à jour en BD.
				* Normalement ça devrait marcher (on vient de
				* récupérer l'equipe). */
				$ok = $this->equipeTab->update($id, $e);
				if (!$ok)
					throw new Exception("Identifier has disappeared?!");
				/* Préparation de la page de l equipe */
				$_SESSION['currentNewEquipe']='';
				$this->view->displayEquipeModifiedPage($id);
				//$this->view->makeEquipePage($id, $e);
			} else {
				$_SESSION['currentNewEquipe']=$cf;
				$this->view->displayEquipeNotModifiedPage($id);
				//$this->view->makeEquipeModifPage($id, $cf);
			}
		}
	}

}

?>
