<?php


require_once("view/View.php");
require_once("control/Controller.php");
require_once("model/EquipeStorageFile.php");

/*
 * Le routeur s'occupe d'analyser les requêtes HTTP
 * pour décider quoi faire et quoi afficher.
 * Il se contente de passer la main au contrôleur et
 * à la vue une fois qu'il a déterminé l'action à effectuer.
 */
class Router {

    private $vue;
    private $equipeTab; 

	public function main( EquipeStorage $eq ) {
		session_start();

		$feedback = key_exists('feedback', $_SESSION) ? $_SESSION['feedback'] : '';
        $_SESSION['feedback'] = '';
		$this->vue = new View($this,$feedback);
		
        $this->equipeTab=$eq;

        if(sizeof($this->equipeTab->readAll()) >32)
		   $this->equipeTab->reinit();

		$ctl = new Controller($this->vue, $this->equipeTab);
		/* Analyse de l'URL */
		$tri = key_exists('tri', $_GET)? $_GET['tri']: null;
		$EquipeId = key_exists('equipe', $_GET)? $_GET['equipe']: null;
		$action = key_exists('action', $_GET)? $_GET['action']: null;
		//echo "action ".$action ."equipeId" . $EquipeId;
		if ($action === null) {
			/* Pas d'action demandée : par défaut on affiche
	 	 	 * la page d'accueil, sauf si une equipe est demandée,
	 	 	 * auquel cas on affiche sa page. */
			$action = ($EquipeId === null)? "accueil": "voir";
		}
        
	

		try {
			switch ($action) {
			case "voir":
				if ($EquipeId === null) {
					$this->vue->makeUnexpectedErrorPage("Uknown action");
				} else {
					$ctl->showInformation($EquipeId);
				}
				break;

			case "nouveau":
				$ctl->newEquipe();
				break;

			case "sauverNouveau":
				$EquipeId = $ctl->saveNewEquipe($_POST);
				break;
            
            
			case "information":
				$this->vue->makeAproposPage();
				break;
				
			case "supprimer":
				if ($EquipeId === null) {
					$this->vue->makeUnexpectedErrorPage("Uknown action");
				} else {
					$ctl->askEquipeDeletion($EquipeId);
				}
				break;

			case "confirmerSuppression":
				if ($EquipeId === null) {
					$this->vue->makeUnexpectedErrorPage("Uknown action");;
				} else {
					$ctl->deleteEquipe($EquipeId);
				}
				break;

			case "modifier":
				if ($EquipeId === null) {
					$this->vue->makeUnexpectedErrorPage("Uknown action");
				} else {
					$ctl->modifyEquipe($EquipeId);
				}
				break;

			case "sauverModifs":
				if ($EquipeId === null) {
					$this->vue->makeUnexpectedErrorPage("Uknown action");
				} else {
					$ctl->saveEquipeModifications($EquipeId, $_POST);
				}
				break;

			case "listeEquipes":
				if ($tri === null) {
					$this->vue->makeUnexpectedErrorPage("Uknown action");
				} else {
					$ctl->showList($this->equipeTab->readAll(),$tri);
				}
				
				break;

			case "Home":
				$this->vue->makeHomePage();
				break;

			default:
				/* L'internaute a demandé une action non prévue. */
				$this->vue->makeHomePage();
				break;
			}
		} catch (Exception $e) {
			/* Si on arrive ici, il s'est passé quelque chose d'imprévu
	 	 	 * (par exemple un problème de base de données) */
			   $this->vue->makeUnexpectedErrorPage($e);
		}

		/* Enfin, on affiche la page préparée */
		$this->vue->render();
	}

	


	public function getListURL($tri) {
		return "?tri=$tri&amp;action=listeEquipes";
	}


    
	public function getEquipeURL($id) {
		return "?equipe=$id";
	}

	public function getHomeURL() {
		return "?action=Home";
	}

    public function getEquipeCreationURL(){
	return "?action=nouveau";
    }

    public function getEquipeSaveURL(){
		return "?action=sauverNouveau";
	}

	public function getEquipelAskDeletionURL($id){
		return "?equipe=$id&amp;action=supprimer";
	}


	public function getEquipeDeletionURL($id){
		return "?equipe=$id&amp;action=confirmerSuppression";
	}

	public function getEquipeModifURL($id) {
		return "?equipe=$id&amp;action=modifier";
	}

	public function getUpdateModifiedEquipe($id) {
		return "?equipe=$id&amp;action=sauverModifs";
	}
	
	public function getInformationURL(){
		return "?action=information";
		}

		/* Fonction pour le POST-redirect-GET,
 	 * destinée à prendre des URL du routeur
 	 * (dont il faut décoder les entités HTML) */
	public function POSTredirect($url, $feedback) {
		$_SESSION['feedback'] = $feedback;
		header("Location: ".htmlspecialchars_decode($url), true, 303);
		die;
	}

	public function getdb()
	 {return $this->equipeTab;}
	
}



