<?php

require_once("Equipe.php");

interface EquipeStorage {

	/* Insère une nouvelle equipe dans la base. Renvoie l'identifiant
	 * de la nouvelle equipe. */
	public function create(Equipe $c);

	/* Renvoie la equipe d'identifiant $id, ou null
	 * si l'identifiant ne correspond à aucune equipe. */
	public function read($id);

	/* Renvoie un tableau associatif id => Equipe
	 * contenant toutes les equipes de la base. */
	public function readAll();

	/* Met à jour une equipe dans la base. Renvoie
	 * true si la modification a été effectuée, false
	 * si l'identifiant ne correspond à aucune equipe. */
	public function update($id, Equipe $c);

	/* Supprime une equipe. Renvoie
	 * true si la suppression a été effectuée, false
	 * si l'identifiant ne correspond à aucune equipe. */
	public function delete($id);

	/* Vide la base. */
	public function deleteAll();

   
}

?>
