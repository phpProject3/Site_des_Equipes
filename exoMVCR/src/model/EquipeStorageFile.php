<?php





/*
 * Gère le stockage de equipes dans un fichier.
 * Plus simple que l'utilisation d'une base de données,
 * car notre application est très simple.
 */

class EquipeStorageFile implements EquipeStorage {

	/* le ObjectFileDB dans lequel l'instance est enregistrée */
	private $db;

	/* Construit une nouvelle instance, qui utilise le fichier donné
	 * en paramètre. */
	public function __construct() {
		$this->db = new ObjectFileDB("/users/benfara211/tmp/sauvegarde.php" );
	}
    
    public function reinit()
    {
     $this->deleteAll();
     $this->create(new Equipe("Maroc", "Maroc.jpg", "06", "groupe F"));
     $this->create(new Equipe("Breesil", "Bresil.jpg", "22", "groupe G"));
     $this->create(new Equipe("Argentine", "Argentine.jpg", "18", "groupe C"));
     $this->create(new Equipe("Portugal", "Portugal.jpg", "07", "groupe H"));
    }
	/* Insère une nouvelle equipe dans la base. Renvoie l'identifiant
	 * de la nouvelle equipe. */
	public function create(Equipe $c) {
        return $this->db->insert($c);
	}

	/* Renvoie la equipe d'identifiant $id, ou null
	 * si l'identifiant ne correspond à aucune equipe. */
	public function read($id) {
		if ($this->db->exists($id)) {
			return $this->db->fetch($id);
        } else {
			return null;
        }
	}

	/* Renvoie un tableau associatif id => equipe
	 * contenant toutes les equipes de la base. */
	public function readAll() {
		return $this->db->fetchAll();
	}

	/* Met à jour une equipe dans la base. Renvoie
	 * true si la modification a été effectuée, false
	 * si l'identifiant ne correspond à aucune equipe. */
	public function update($id, Equipe $c) {
		if ($this->db->exists($id)) {
            $this->db->update($id, $c);
			return true;
		}
		return false;
	}

	/* Supprime une equipe. Renvoie
	 * true si la suppression a été effectuée, false
	 * si l'identifiant ne correspond à aucune equipe. */
	public function delete($id) {
		if ($this->db->exists($id)) {
			$this->db->delete($id);
			return true;
		}
		return false;
	}

	/* Vide la base. */
	public function deleteAll() {
        $this->db->deleteAll();
	}
}


