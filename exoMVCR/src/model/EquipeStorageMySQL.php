<?php
require_once("model/Equipe.php");
require_once("model/EquipeStorage.php");

/* Une classe de démo de l'architecture. Une vraie BD ne contiendrait
 * évidemment pas directement des instances de Poem, il faudrait
 * les construire lors de la lecture en BD. */
class EquipeStorageMySQL implements EquipeStorage {

	protected $db;

	/* Construit une instance avec des equipes . */
	public function __construct($pdo) {
       
        $this->db=$pdo;
	}

	function reinit() {
		$sql = "delete * from equipes;";
		$this->db->exec($sql);
		$equipes = array(
			array(
				'id' => 1,
				'equipeNationale' => 'Maroc',
				'grp' => 'D',
				'nbrPar' => '6'
				),
			array(
				'id' => 2,
				'equipeNationale' => 'Argentine',
				'grp' => 'F',
				'nbrPar' => '18'
				),
			array(
				'id' => 3,
				'equipeNationale' => 'Bresil',
				'grp' => 'G',
				'nbrPar'=> '21'
				),
				array(
					'id' => 4,
					'equipeNationale' => 'Portugal',
					'grp' => 'H',
					'nbrPar'=> '7'
				),

			);
			foreach($equipes as $tab)
			{
				$rq = "insert into equipes (id,equipeNationale,nbrPar,grp) value ('".$tab['id']."','".$tab['equipeNationale']."','".$tab['nbrPar']."','".$tab['grp']."' );";
				$stmt = $this->db->prepare($rq);
				$stmt->execute();
	  
			}
		}

	public function read($id) {
		$rq = "SELECT * FROM equipes WHERE id=$id";
		$stmt = $this->db->prepare($rq);
		$stmt->execute();
		$result=$stmt->fetch();
		if($result === false)
		  return null;
		switch ($result['equipeNationale']) {
			case 'Maroc':
				$image="Maroc.jpg";
				break;

			case 'Bresil':
				$image="Bresil.jpg";
				break;

			case 'Argentine':
				$image="Argentine.jpg";
				break;

			case 'Portugal':
				$image="Portugal.jpg";
				break;
			default : 
			   $image="coupe.jpg";
			   break;
			}
		return new Equipe($result['equipeNationale'],$image,$result['nbrPar'],$result['grp']);
		
	}

	public function readAll() {
		$rq ="SELECT * FROM equipes";
        $stmt = $this->db->prepare($rq);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $donnees = array();
		//var_dump($result);
		foreach($result as $value){
			
			$donnees[$value['id']] = new Equipe($value['equipeNationale'],'',$value['nbrPar'],$value['grp']);
		}
		return $donnees;
		
	}

	public function update($id, Equipe $c) {
        $nom =$this->db->quote($c->getNom());
		$nbr =$this->db->quote($c->getNbr_particip());
		$groupe =$this->db->quote($c->getGroupe());

		$rq="UPDATE equipes SET equipeNationale=".$nom.", nbrPar=".$nbr.", grp=".$groupe." WHERE id = '".$id."'";
		$this->db->exec($rq);
		return $this->db->quote($this->readAll()[$id]->getNbr_particip()) == $nbr ; 

		//throw new Exception("not yet implemented");

	}

	/* Supprime une Equipe. Renvoie
	 * true si la suppression a été effectuée, false
	 * si l'identifiant ne correspond à aucune Equipe. */
	public function delete($id) {
        $rq = "DELETE FROM equipes WHERE id = '$id'";
		$this->db->exec($rq);
		//throw new Exception("not yet implemented");
	}

	/* Vide la base. */
	public function deleteAll() {
		$sql = "delete * from equipes;";
		$this->db->exec($sql);
       //throw new Exception("not yet implemented");
	}

	public function create(Equipe $c) {
     
	  $nom =$this->db->quote($c->getNom());
	  $nbr =$this->db->quote($c->getNbr_particip());
	  $groupe =$this->db->quote($c->getGroupe());

      //echo $nom . ' et '.$nbr . ' et '.$groupe;
	  $rq1="INSERT INTO equipes (equipeNationale,nbrPar,grp) VALUES ($nom,$nbr,$groupe)";
		$this->db->exec($rq1);
        // var_dump($this->readAll());
		$rq2 = "SELECT id FROM equipes WHERE equipeNationale =$nom;";
		$stmt = $this->db->prepare($rq2);
		$stmt->execute();
		$res = $stmt->fetch();
		//var_dump($res);
		return $res['id'];
		/*$rq = "SELECT * FROM equipes WHERE id=$id";
		$stmt = $this->db->prepare($rq);
		$stmt->execute();
		$result=$stmt->fetch();
		if($result === false)
		  return null;*/
		
		//throw new Exception("not yet implemented");
	}
}

