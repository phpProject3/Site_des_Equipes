<?php

/* Représente une equipe nationale. */
class Equipe {

	protected $nom;
	protected $nbr_particip;
    protected $groupe;
	//protected $text;
    protected $image;

	public function __construct($nom, $image, $nbr_particip,$groupe) {
		$this->nom = $nom;
        $this->groupe=$groupe;
        $this->nbr_particip = $nbr_particip;
		$this->image = $image;
		
	}

	/* Renvoie le titre */
	public function getNom() {
		return $this->nom;
	}

	/* Renvoie l'image */
	public function getImage() {
		return $this->image;
	}
    
	/* Renvoie le nombre de participation */
	public function getNbr_particip() {
		return $this->nbr_particip;
	}
    
    public function getGroupe() {
		return $this->groupe;
	}
    
	public function setName($name){
	  $this->nom=$name;
	}

	public function setGroupe($groupe){
		$this->groupe=$groupe;
	}

	public function setNbr_particip($nbr_particip){
		$this->nbr_particip = $nbr_particip;
	}



}

?>
