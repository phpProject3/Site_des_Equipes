<?php
require_once("model/Equipe.php");
require_once("model/EquipeStorage.php");

/* Une classe de démo de l'architecture. Une vraie BD ne contiendrait
 * évidemment pas directement des instances de Poem, il faudrait
 * les construire lors de la lecture en BD. */
class EquipeStorageStub implements EquipeStorage {

	protected $equipeTab;

	/* Construit une instance avec des equipes . */
	/*public function __construct() {
        $this->equipeTab =  array( 
            "1" => new Equipe("Maroc", "Maroc.jpg", "06", "groupe F"),
            "2" => new Equipe("Breesil", "Bresil.jpg", "22", "groupe G"),
            "3" => new Equipe("Argentine", "Argentine.jpg", "18", "groupe C"),
            "4" => new Equipe("Portugal", "Portugal.jpg", "07", "groupe H"),
        );
	}*/
	

	public function read($id) {
		if (in_array($id, array_keys($this->equipeTab))) {
			return $this->equipeTab[$id];
		}
		return null;
	}

	public function readAll() {
		return $this->equipeTab;
	}

	public function update($id, Equipe $c) {
		/*if ($this->db->exists($id)) {
            $this->db->update($id, $c);
			return true;
		}
		return false;*/
	}

	/* Supprime une couleur. Renvoie
	 * true si la suppression a été effectuée, false
	 * si l'identifiant ne correspond à aucune couleur. */
	public function delete($id) {
		/*if ($this->db->exists($id)) {
			$this->db->delete($id);
			return true;
		}
		return false;*/
	}

	/* Vide la base. */
	public function deleteAll() {
      // $this->db->deleteAll();
	}

	public function create(Equipe $c) {
        //return $this->db->insert($c);
	}
}

