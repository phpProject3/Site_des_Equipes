<?php

require_once("model/Equipe.php");

class EquipeBuilder {

	protected $data;
	protected $errors;

	/* Crée une nouvelle instance, avec les données passées en argument si
 	 * elles existent, et sinon avec
 	 * les valeurs par défaut des champs de création d'une equipe. */
	public function __construct($data=null) {
		if ($data === null) {
			$data = array(
                "equipeNationale" => "",
                "nbrPar" =>  "",
                "grp" => "",
                
            );
		}
		$this->data = $data;
		$this->errors = array();
	}

	/* Renvoie une nouvelle instance de EquipeBuilder avec les données
 	 * modifiables de l'equipe passée en argument. */
	public static function buildFromEquipe(Equipe $equipe) {
		return new EquipeBuilder(array(
            "equipeNationale" => $equipe->getNom(),
                "nbrPar" => $equipe->getNbr_particip(),
                "grp" => $equipe->getGroupe(),
			
		));
	}

	/* Vérifie la validité des données envoyées par le client,
	 * et renvoie un tableau des erreurs à corriger. */
    public function isValid() {
        $errors =array();
        $data=$this->data;
       if (empty($data['equipeNationale']) || !is_string($data['equipeNationale']) || (preg_match('/[\'123456789^£$%&*()}!°{@#~?><,|=_+¬-]/', $data['equipeNationale'])))
           $errors['equipeNationale']= "Vous devez entrer un nom valide";
       else if (mb_strlen($data["equipeNationale"], 'UTF-8') >= 20)
           $errors['equipeNationale'] = "Le nom doit faire moins de 20 caractères";
       else if (!is_numeric($data['nbrPar']) || $data['nbrPar'] === "")
           $errors['nbrPar'] = "Vous devez entrer un nombre";
       else if (intval($data['nbrPar']) > 21)
           $errors['nbrPar'] = "Y a que 21 coupes du Monde organisées";
       else if (!is_string($data['grp']) || strlen($data['grp'])!==1 || is_numeric($data['grp']) )
           $errors['grp'] = "Vous devez entrer exactement 1 Caractère entre A et H ";
        $this->errors =$errors;
        return empty($this->errors);

   }

	/* Renvoie la «référence» du champ représentant le nom d'une equipe. */
	public function getNameRef() {
		return "equipeNationale";
	}

	/* Renvoie la «référence» du champ représentant le nombre de participation d'une equipe. */
	public function getNbrPartiRef() {
		return "nbrPar";
	}

    public function getGroupeRef() {
		return "grp";
	}

	/* Renvoie la valeur d'un champ en fonction de la référence passée en argument. */
	public function getData($ref) {
		return key_exists($ref, $this->data)? $this->data[$ref]: '';
	}

	/* Renvoie les erreurs associées au champ de la référence passée en argument,
 	 * ou null s'il n'y a pas d'erreur.
 	 * Nécessite d'avoir appelé isValid() auparavant. */
	public function getErrors($ref) {
		return key_exists($ref, $this->errors)? $this->errors[$ref]: null;
	}

	/* Crée une nouvelle instance de Equipe avec les données
	 * fournies. Si toutes ne sont pas présentes, une exception
	 * est lancée. */
	public function createEquipe() {
		if (!key_exists("equipeNationale", $this->data) || !key_exists("nbrPar", $this->data) || !key_exists("grp", $this->data) )
			throw new Exception("Missing fields for Equipe creation");
		return new Equipe($this->data['equipeNationale'],'',$this->data['nbrPar'],$this->data['grp']);
	}

	/* Met à jour une instance de Equipe avec les données
	 * fournies. */
	public function updateEquipe(Equipe $eq) {

		if (key_exists("equipeNationale", $this->data))
			$eq->setName($this->data["equipeNationale"]);
		if (key_exists("grp", $this->data))
			$eq->setGroupe($this->data["grp"]);
        if (key_exists("nbrPar", $this->data))
            $eq->setNbr_Particip($this->data["nbrPar"]);
	}

}

?>
