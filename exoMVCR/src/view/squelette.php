<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?php echo $title ?></title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="skin/skin.css" />
</head>
<body>
    <nav class="menu">
        <ul>
<?php
    foreach ($listeEquipes as $equipeName => $url) {
        echo "<li><a href=\"$url\">$equipeName</a></li>";
    }
?>
        </ul>
    </nav>
    <main>
        <h2><?php echo $feedback ; ?></h2>
        <h1><?php echo $title; ?></h1>
        
        <p><?php echo $content; ?></p>
    
    </main>
</body>


</html>