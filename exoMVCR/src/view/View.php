<?php

require_once("Router.php");
require_once("model/Equipe.php");
class View {

	
	protected $title;
	protected $content;
	protected $router;
    protected $feedback;

	public function __construct(Router $router,$feedback) {
		$this->router = $router;
		$this->title = null;
		$this->content =null ;
		$this->feedback = $feedback;

	}

	public function setTitleContent($title,$content)
	  {$this->title = $title;
		$this->content =$content ;
	  }

	/* Affiche la page créée. */
	public function render() {
		if ($this->title === null || $this->content === null) {
			$this->title = "Erreur";
		    $this->content = "Une erreur inattendue s'est produite.";
		}
		
	   $title=$this->title;
	   $content=$this->content;
	   $feedback = $this->feedback;
	   $listeEquipes = array(
		"Accueil" => $this->router->getHomeURL(),
		"Liste des equipes" =>  $this->router->getListURL(0),
	    "Nouvelle Equipe" => $this->router->getEquipeCreationURL(),
	    "A Propos" => $this->router->getInformationURL(),
		   );

	  
		include("squelette.php");
	}


	/******************************************************************************/
	/* Méthodes de génération des pages                                           */
	/******************************************************************************/
    public function makeTestPage() {
		$this->title = "Laaarbi !";
		$this->content = "Yorahiboo biKoooM.";
		
	}
    public function makeHomePage() {
		$this->title = "Bienvenue !";
		$this->content = "Un site sur des equipes nationales participantes a la coupe du monde 2022 :";
		
	}
	public function makeEquipePage($id,$equipe) {
		$ptitle =$equipe->getNom();
		$this->content =$ptitle . " participe pour la ".$equipe->getNbr_particip()."eme fois dans la coupe du Monde , se trouve dans le groupe ".$equipe->getGroupe()."\n";
		
		$groupe =$equipe->getGroupe();
		$image = "images/{$equipe->getImage()}";

		$this->title = "/////// « {$ptitle} », Groupe $groupe ///////";
		$this->content .= "<figure>\n<img src=\"$image\" alt=\"$groupe\" />\n";
		$this->content .= "<figcaption> $ptitle football team</figcaption>\n</figure>\n";
		$this->content .= "<ul>\n";
		$this->content .= "<li style="."'background-color: rgb(144, 207, 207);'".'><a href="'.$this->router->getEquipeModifURL($id).'">Modifier</a></li>'."\n";
		$this->content .= "<li style="."'background-color: rgb(144, 207, 207);'".'><a href="'.$this->router->getEquipelAskDeletionURL($id).'">Supprimer</a></li>'."\n";
		$this->content.= "</ul>\n";
		
	}

    public function makeUnknownEquipePage() {
		$this->title = "Erreur";
		$this->content = "Equipe Inconnu";
		
	}
	public function makeUnexpectedErrorPage($error) {
		$this->title = "Erreur :". $error;
		$this->content = "Equipe Inconnu";
		
	}
	
	public function makeListPage($equipeTabl) {
		$this->content.= "<nav>";
        $this->content .= "<ul  style="."'background-color: rgb(144, 207, 207);'" . ">";
		

		
		foreach( $equipeTabl as $key => $value)
		 { 
			$this->content .= '<li>'.'<a href= '.$this->router->getEquipeURL($key).">".$value->getNom()  ."</a>".'</li>'."\n";
		}
        
		$this->title="liste des equipes";
	    $this->content.= "</ul>";
		$this->content.= '<form action="'.$this->router->getListURL(1).'" method="POST">'."\n";
		$this->content.= "<button>Tri Croissant</button>\n";
		$this->content.= "</form>\n";
		$this->content.= '<form action="'.$this->router->getListURL(-1).'" method="POST">'."\n";
		$this->content.= "<button>Tri Decroissant</button>\n";
		$this->content.= "</form>\n";
		$this->content.= "</nav>";
        
	}


	public function makeDebugPage($variable) {
		$this->title = 'Debug';
		$this->content = '<pre>'.htmlspecialchars(var_export($variable, true)).'</pre>';
	}
	
	public function makeEquipeCreationPage( EquipeBuilder $equipeb){
        
			$this->title = "Ajouter votre Equipe Nationale participante à la coupe du Monde";
			$s = '<form action="'.$this->router->getEquipeSaveURL().'" method="POST">'."\n";
			$s .= self::getFormFields($equipeb);
			$s .= "<button>Créer</button>\n";
			$s .= "</form>\n";
			$this->content = $s;
		} 

    public function makeEquipeDeletionPage($id, Equipe $eq) {
		$cname = self::htmlesc($eq->getNom());

		$this->title = "Suppression de l equipe $cname";
		$this->content = "<p>L'equipe « {$cname} » va être supprimée.</p>\n";
		$this->content .= '<form action="'.$this->router->getEquipeDeletionURL($id).'" method="POST">'."\n";
		$this->content .= "<button>Confirmer</button>\n</form>\n";
	}

	public function makeEquipeDeletedPage() {
		$this->title = "Suppression effectuée";
		$this->content = "<p>L'equipe a été correctement supprimée.</p>";
	}

	public function makeEquipeModifPage($id, EquipeBuilder $builder) {
		$this->title = "Modifier l equipe";

		$this->content = '<form action="'.$this->router->getUpdateModifiedEquipe($id).'" method="POST">'."\n";
		$this->content .= self::getFormFields($builder);
		$this->content .= '<button>Modifier</button>'."\n";
		$this->content .= '</form>'."\n";
	}

	public function displayEquipeCreationSuccess($id){
		$this->router->POSTredirect($this->router->getEquipeURL($id), "L'equipe a bien été créée !");
	}
	
	public function displayEquipeCreationFailure(){
		$this->router->POSTredirect($this->router->getEquipeCreationURL(), "Erreurs dans le remplissement du formulaire.!");
	}

	public function displayEquipeDeletedPage($id) {
		$this->router->POSTredirect($this->router->getListURL(0), "L'equipe a été correctement supprimée!");
	}
    
	public function displayEquipeModifiedPage($id) {
		$this->router->POSTredirect($this->router->getEquipeURL($id), "L equipe a bien été modifiée !");
	}

	public function displayEquipeNotModifiedPage($id) {
		$this->router->POSTredirect($this->router->getEquipeModifURL($id), "Erreurs dans le remplissement du formulaire.");
	}
	
	 public function makeAproposPage() {
		$this->title = "A PROPOS !";
		$this->content .= "<nav style="."'background-color: rgb(144, 207, 207);'" .">" ;
		$this->content .= "Numéro d'Etudiant : 22110518" ."<br>";
		$this->content .=  "les points realisés : " ;
		$this->content .= "<ul>";
		$this->content .= '<li>'    ."Liste d objets, affichables indépendamment  ".  '</li>'."\n";
        $this->content .= '<li>'     ."Modification basique d objets ".    '</li>'."\n";
		$this->content .= '<li>'   ." Builders pour la manipulation d objets ".   '</li>'."\n";
		$this->content .= '<li>'    ."Suppression d objets  ".  '</li>'."\n";
		$this->content .= '<li>'    ."Redirection en GET après création/modif/suppression réussie ".   '</li>'."\n";
		$this->content .= '<li>'    ."Gestion du feedback ".  '</li>'."\n";
		$this->content .= '<li>'   ."Redirection en GET après POST même lors des erreurs ".   '</li>'."\n";
		$this->content .= '<li>'   ."possibilité de modifier l'ordre d'affichage de la liste (tri par ordre alphabetique)  ".   '</li>'."\n";
		$this->content .= '<li>'   ."Utilisation d une base de données MySQL  ".   '</li>'."\n";
		$this->content .= '<li>'   ." chaque erreur s'affiche à côté du champ concerné.".   '</li>'."\n";

		$this->content.= "</ul>";
		$this->content.=" Pour le design , c'est une simple réalisation css, vu que c'était pas demandé, j'ai donné plus de temps au fonctionnement du site. <br> " ;
        $this->content.="</nav>";
		
		
		
	}
	/////////////////////////////////////////////////////////////

	protected function getFormFields(EquipeBuilder $builder) {
		$nameRef = $builder->getNameRef();
		$s = "";

		$s .= '<p><label>Nom d equipe Nationale: <input type="text" name="'.$nameRef.'" value="';
		$s .= self::htmlesc($builder->getData($nameRef));
		$s .= "\" />";
		$err = $builder->getErrors($nameRef);
		if ($err !== null)
			$s .= ' <span class="error">'.$err.'</span>';
		$s .="</label></p>\n";

		$NbrParRef = $builder->getNbrPartiRef();
		$s .= '<p><label>Nombre de Participation  : <input type="text" name="'.$NbrParRef.'" value="';
		$s .= self::htmlesc($builder->getData($NbrParRef));
		$s .= '" ';
		//$s .= 'pattern="[0-9a-fA-F]{6}"';
		$s .= '	/>';
		$err = $builder->getErrors($NbrParRef);
		if ($err !== null)
			$s .= ' <span class="error">'.$err.'</span>';
		$s .= '</label></p>'."\n";

		$grpRef = $builder->getGroupeRef();
		$s .= '<p><label>Groupe a Qatar 2022 : <input type="text" name="'.$grpRef.'" value="';
		$s .= self::htmlesc($builder->getData($grpRef));
		$s .= '" ';
		//$s .= 'pattern="[0-9a-fA-F]{6}"';
		$s .= '	/>';
		$err = $builder->getErrors($grpRef);
		if ($err !== null)
			$s .= ' <span class="error">'.$err.'</span>';
		$s .= '</label></p>'."\n";
		return $s;
	}
	public static function htmlesc($str) {
		return htmlspecialchars($str,
			/* on échappe guillemets _et_ apostrophes : */
			ENT_QUOTES
			/* les séquences UTF-8 invalides sont
			* remplacées par le caractère �
			* au lieu de renvoyer la chaîne vide…) */
			| ENT_SUBSTITUTE
			/* on utilise les entités HTML5 (en particulier &apos;) */
			| ENT_HTML5,
			'UTF-8');
	}
}

?>
